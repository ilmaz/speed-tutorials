************************************************************************************************
TUTORIALS contains different examples of elastic wave problem to get practice with SPEED.
************************************************************************************************
Run these example on tour machine as explained in the online documentation.

PLANE_WAVE: 
      Plane wave propagation problem. 
      
TEST_CASE_LOH1: Benchmark tectcase Layer over a half space (LOH1) described in: 
      "Day S.M., Bradley C.R. 2001. Memory-efficient simulation of anelastic wave 
      propagation. Bulletin of the Seismological Society of America, 91(3): 520–531." 
      
CROISSANT_VALLEY: Benchmark testcase proposed by "F.J.Sanchez-Sesma & F.Luzon (1995). 
      Seismic Response of a Three-Dimensional Alluvial Valleys for Incident P, 
      S and Rayleigh Waves. Bulletin of the Seismological Society of America, 85, 890-899."
      
MONTELIMAR: Simplification of the testcase proposed by Smerzini, C., Vanini, M., 
      Paolucci, R. et al. Regional physics-based simulation of ground motion 
      within the Rhȏne Valley, France, during the MW 4.9 2019 Le Teil earthquake. 
      Bull Earthquake Eng (2022).      
      
