OUTPUTS:

SPEED will generate automatically three different category of files

- Files with extension ".mpi"  will be stored (if specified in SPEED.input, see online documentation) in the folder 
FILES_MPI otherwise will be sorted in the working directory. Such files are needed for managing the parallel computation and
can be used for furhter runs (see Further Runs).

- Files with extension ".input" are created during the first run and are read in successive runs. 

- Files MONITOR*.INFO e MONITOR*.D (e/o .V,.A,.S,.E,.O) contain the numerical solution and are stored in the folder
MONFILE (if specified in SPEED.input, see online documentation) otherwise will be stored in the working directory. 


POST-PROCESSING:
Run the Matlab script SesmaLuzonVsSPEED.m


NOT-HONORING ENHANCED OPTION! 

Example for Not-Honoring Enhanced (NHE), inside the folder 'Simulations/5NotHonoringEnhancedSEM'.

For the NHE approach, the user creates an input file 'tomo_xyz_mech.in'. 
This file contains mechanical properties (Vs, Vp, density etc...) at a grid/cloud of points inside the simulation domain. 
These cloud of point, need not coincide with the input spectral element nodes. 
At the backend of SPEED, for each spectral node in MESH, we compute the nearest possible point in the 'input point cloud', and corresponding mechanical properties are assigned to spectral node. In the simulation folder 'Make_croissant_valley_tomo_in_file.m' script will show an example on how to create a simple 'tomo_xyz_mech.in' file for this testcase.
