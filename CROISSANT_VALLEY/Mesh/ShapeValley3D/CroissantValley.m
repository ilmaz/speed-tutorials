%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano
%         P.zza Leonardo da Vinci, 32
%         20133 Milano
%         Italy
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************
%% Program CroissantValley.m
%
%
%    Plot of the "Croissant-Valley" as described in  
%
%    References: 
%    [1] F.J.Sanchez-Sesma & F.Luzon (1995) "Seismic Response of a Three-Dimensional
%             Alluvial Valleys for Incident P, S and Rayleigh Waves" Bull.
%             Seism. Soc. Am. , 85 , 890-899
%    [2] I.Mazzieri (2012) "Non-Conforming High Order Methods for the
%             Elastodynamics Equation", Ph.D. Thesis, Politecnico di Milano.
%    [3] M.Stupazzini (2004) "A Spectral Element Approach for 3D Dynamic Soil
%             Structure Interaction Problems", Ph.D. Thesis, Politecnico di Milano.
%

clear; clc; close all; warning off;

%% Parameters
path='';
file_name='CroissantValley';

a = 4000; % [m]
b = .7*a; % [m]
h = .4/a; % [m]


tic
%% Computation of the CroissantValley
x_min=-4*a;
x_max=4*(a);
x_step=(x_max-x_min)/200;

y_min=-4*a;
y_max=4*a;
y_step=(y_max-y_min)/200;

x = [x_min:x_step:x_max];
y = [y_min:y_step:y_max];

[X,Y] = meshgrid(x,y);

r2 = X.^2+Y.^2;
R2 = (X-a).^2+Y.^2;
Z = h.*(b.^2-R2).*(1-2.*a.*(a-X)./R2);

for i=1:length(X)
    for j=1:length(Y)
        if (r2(i,j)^.5<a)&(R2(i,j)^.5>b)
            Z(i,j)=Z(i,j);
        else
            Z(i,j)=0;
        end
    end
end

surface(X,Y,-Z);
colormap summer;
shading flat;
view(-30,60);

zlim([-.3*a .3*a]);  



%% Writing the Output
max_ij=10^9;

if i*j>max_ij
    
    for i=1:length(X)
            for j=1:length(Y)
                x((i-1)*length(X)+j)=X(i,j);
                y((i-1)*length(Y)+j)=Y(i,j);
                z((i-1)*length(Z)+j)=Z(i,j);
            end
    end
    
    for k =1:ceil(i*j/max_ij)
    nomefilew=[char(path),char(file_name),char(num2str(k)),'.xyz'];
    fid = fopen(nomefilew,'w');
    
        h_ini=(k-1)*max_ij+1;
        if k==ceil(i*j/max_ij)
            h_end=i*j;
        else
            h_end=(k-1)*max_ij+max_ij;
        end
        
        for h=h_ini:h_end
           fprintf(fid,'%+13.7e  %+13.7e  %+13.7e\n',[x(h); y(h); z(h)]);
           %fprintf(fid,'%+8.4f  %+8.4f  %+8.4f\n',[x(h); y(h); z(h)]);
        end
        fprintf(fid,'\n');
    fclose(fid);
    end
    
else
    nomefilew=[char(path),char(file_name),'.xyz'];
    fid = fopen(nomefilew,'w');
        for i=1:length(X)
            for j=1:length(Y)
                fprintf(fid,'%+8.4f  %+8.4f  %+8.4f\n',[X(i,j); Y(i,j); Z(i,j)]);
            end
        end
        fprintf(fid,'\n');
    fclose(fid);
end

toc

%% End of the Program