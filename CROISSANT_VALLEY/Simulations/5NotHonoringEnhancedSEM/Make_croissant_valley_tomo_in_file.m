%% Program CroissantValley.m
%
%
%    Plot of the "Croissant-Valley" as described in  
%
%    References: 
%    [1] F.J.Sanchez-Sesma & F.Luzon (1995) "Seismic Response of a Three-Dimensional
%             Alluvial Valleys for Incident P, S and Rayleigh Waves" Bull.
%             Seism. Soc. Am. , 85 , 890-899
%    [2] I.Mazzieri (2012) "Non-Conforming High Order Methods for the
%             Elastodynamics Equation", Ph.D. Thesis, Politecnico di Milano.
%    [3] M.Stupazzini (2004) "A Spectral Element Approach for 3D Dynamic Soil
%             Structure Interaction Problems", Ph.D. Thesis, Politecnico di Milano.
%

clear; clc; close all; warning off;

%% Parameters
path='';
file_name='CroissantValley';

a = 4000; % [m]
b = .7*a; % [m]
h = .4/a; % [m]


tic
%% Computation of the CroissantValley
x_min=-10000;
x_max=10000;
x_step=200;

y_min=-10000;
y_max=10000;
y_step=200;

z_min = -7000;
z_max = 0;
z_step = 200;

x = [x_min:x_step:x_max];
y = [y_min:y_step:y_max];
zcord = [z_min:z_step:z_max];

[X,Y] = meshgrid(x,y);

%% Basin Depth
r2 = X.^2+Y.^2;
R2 = (X-a).^2+Y.^2;
Z = h.*(b.^2-R2).*(1-2.*a.*(a-X)./R2);

for i=1:size(X,1)
    for j=1:size(X,2)
        if (r2(i,j)^.5<a)&&(R2(i,j)^.5>b)
            Z(i,j)=Z(i,j);
        else
            Z(i,j)=0;
        end
    end
end

Z=-Z;

surface(X,Y,Z);
colormap summer;
shading flat;
view(-30,60);

% zlim([-6000 6000]);  

%% Generating Tomo Data
nz = length(zcord);
ny = size(X,1);
nx = size(X,2);

grid_tomo = zeros(nz*nx*ny,8);
count = 0;

rho = [2.0 2.5]*1000;
vs = [1.0 2.0]*1000;
vp = [2.0819942 3.4639976]*1000;
qs = [100 100];
qp = [200 200];

for k=1:nz
    for j=1:nx
        for i=1:ny
            count = count +1;
            grid_tomo(count,1) = X(i,j);
            grid_tomo(count,2) = Y(i,j);
            grid_tomo(count,3) = zcord(k);
            
            if Z(i,j) == 0
                if (r2(i,j)^.5<a)&&(R2(i,j)^.5>b)
                    % Points at Basin Top edge
                    grid_tomo(count,4) = rho(1);
                    grid_tomo(count,5) = vs(1);
                    grid_tomo(count,6) = vp(1);
                    grid_tomo(count,7) = qs(1);
                    grid_tomo(count,8) = qp(1);
                else
                    grid_tomo(count,4) = rho(2);
                    grid_tomo(count,5) = vs(2);
                    grid_tomo(count,6) = vp(2);
                    grid_tomo(count,7) = qs(2);
                    grid_tomo(count,8) = qp(2);
                end
            else
                if (zcord(k) - Z(i,j)) >= 0 %Point inside basin
                    grid_tomo(count,4) = rho(1);
                    grid_tomo(count,5) = vs(1);
                    grid_tomo(count,6) = vp(1);
                    grid_tomo(count,7) = qs(1);
                    grid_tomo(count,8) = qp(1);
                else
                    grid_tomo(count,4) = rho(2);
                    grid_tomo(count,5) = vs(2);
                    grid_tomo(count,6) = vp(2);
                    grid_tomo(count,7) = qs(2);
                    grid_tomo(count,8) = qp(2);
                end
            end
            
        end
    end
end

% Plotting

hold on;

ind_b1 = find(grid_tomo(:,5) == 1000);
scatter3(grid_tomo(ind_b1(:,1),1),grid_tomo(ind_b1(:,1),2),grid_tomo(ind_b1(:,1),3),50)

hold on
ind_b1 = find(grid_tomo(:,5) == 2000);
scatter3(grid_tomo(ind_b1(:,1),1),grid_tomo(ind_b1(:,1),2),grid_tomo(ind_b1(:,1),3),50)


%% Writing the Output
fid=fopen('./tomo_xyz_mech.in','w');
fprintf(fid,'%10s\t%10s\t%10s\r\n','Easting','Northing','Z-dir');
fprintf(fid,'%i\r\n',size(grid_tomo,1));
fprintf(fid,'%10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f %10.2f\r\n',grid_tomo');
fclose(fid);