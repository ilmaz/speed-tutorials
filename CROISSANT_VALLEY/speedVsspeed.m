%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano
%         P.zza Leonardo da Vinci, 32
%         20133 Milano
%         Italy
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**********************************************************************
%% speedVsspeed 
%
%    Comparison between different solutions by SPEED 

dir1 = './mytest/1ConformingSEM/MONITORS/MONITORS_REWRITE/';
dir2 = './mytest/5NotHonoringEnhancedSEM/MONITORS/MONITORS_REWRITE/';

station=[49:-1:1];

scala = 1;
figure();
for imon = 1:length(station)
    if station(imon)<10
        fname = 'monitor0000';
    else
        fname = 'monitor000';
    end
    a = load(sprintf('%s%s%i.d',dir1,fname,station(imon)));
    a2 = load(sprintf('%s%s%i.d',dir2,fname,station(imon)));
    
    t = a(:,1);
    dt = t(2) - t(1);
    ux = a(:,2);
    uy = a(:,3);
    uz = a(:,4);
    
    t2 = a2(:,1);
    dt2 = t2(2) - t(1);
    ux2 = a2(:,2);
    uy2 = a2(:,3);
    uz2 = a2(:,4);
    
    plot(t-3.50,uy.*(-.5).*scala+(imon+1)/2,'b-','LineWidth',1.5);hold on;
    plot(t2-3.50,uy2.*(-.5).*scala+(imon+1)/2,'r-','LineWidth',1.5);hold on;
    xlim([0 25]);
    ylim([0 25.6]);
    
    if imon==1
        xlabel('Time [s]','FontSize',12);
    elseif imon==length(station)
        title('Valley - SH \gamma = 0','FontSize',12);
    end
end
ylabel('u_y(x,0,0,t)','FontSize',12,'HorizontalAlignment','right','Rotation',90);

%%

station=[49:1:98];

figure;
for imon = 1:length(station)
    if station(imon)<10
        fname = 'monitor0000';
    else
        fname = 'monitor000';
    end
    a = load(sprintf('%s%s%i.d',dir1,fname,station(imon)));
    a2 = load(sprintf('%s%s%i.d',dir2,fname,station(imon)));
    
    t = a(:,1);
    dt = t(2) - t(1);
    ux = a(:,2);
    uy = a(:,3);
    uz = a(:,4);
    
    t2 = a2(:,1);
    dt2 = t2(2) - t(1);
    ux2 = a2(:,2);
    uy2 = a2(:,3);
    uz2 = a2(:,4);
    
    plot(t-3.50,uy.*(-.5).*scala+(imon+1)/2,'b-','LineWidth',1.5);hold on;
    plot(t2-3.50,uy2.*(-.5).*scala+(imon+1)/2,'r-','LineWidth',1.5);hold on;
    xlim([0 25]);
    ylim([0 25.6]);
    
    if imon==1
        xlabel('Time [s]','FontSize',12);
    elseif imon==length(station)
        title('Valley - SH \gamma = 0','FontSize',12);
    end
end
ylabel('u_y(0,y,0,t)','FontSize',12,'HorizontalAlignment','right','Rotation',90);
