%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano
%         P.zza Leonardo da Vinci, 32
%         20133 Milano
%         Italy
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************
%% SesmaLuzonVsSPEED(DirectoryName)
%
%    Comparison between semi-analytical solution (IBEM) and SPEED for the 
%    "Croissant-Valley" test case 
%
%    References: 
%    [1] F.J.Sanchez-Sesma & F.Luzon (1995) "Seismic Response of a Three-Dimensional
%             Alluvial Valleys for Incident P, S and Rayleigh Waves" Bull.
%             Seism. Soc. Am. , 85 , 890-899
%    [2] I.Mazzieri (2012) "Non-Conforming High Order Methods for the
%             Elastodynamics Equation", Ph.D. Thesis, Politecnico di Milano.
%    [3] M.Stupazzini (2004) "A Spectral Element Approach for 3D Dynamic Soil
%             Structure Interaction Problems", Ph.D. Thesis, Politecnico di Milano.
%
%    INPUT:
%       DirectoryName  : directory where SPEED output files are saved
%       (string)
%
%    OUTPUT:
%       Comparison of time-histories obatined by IBEM (blue) vs SPEED (red)
%
%
% e.g.: 
% SesmaLuzonVsSPEED('Input/2ConformingDG/MONITORS/')

function SesmaLuzonVsSPEED(DirectoryName)

close all; warning off; clc;
addpath SrcMatlab


%% Rewrite SPEED output

ConvertMONITORS(DirectoryName,DirectoryName);


%% Semi-Analytical Solution from Sesma&Luzon X-Component
file_nameXY='SINTSH0.SYY';
filename_read=['SesmaLuzon/',file_nameXY];
a = load(filename_read);

% Parameters SesmaLuzon 
dt=0.62992;
t=[0:255].*dt;
scala = 1;
filtra = 0;
n = 3;
freq_low=.01; freq_high=0.7;


figure(10);
for k=1:(length(a)/256)
    a_ini=(k-1)*256+1;
    a_end=k*256;
    
    t_int=interp(t,50);
    dt = t_int(2) - t_int(1);
    b = interp(a(a_ini:a_end),50);
    
    if filtra == 1
        fnyq=1/2/dt;
        Nfft=length(b);
        df=1/Nfft/dt;
        freq=[0:df:(Nfft-1)*df];
        Wn = [freq_low freq_high]/fnyq;
        [c,d] = butter(n,Wn);
        
        b = filter(c,d,b);
    end
    
    if (k>1) && (k<(length(a)/256))
        plot(t_int,b+k/2,'b-','LineWidth',2);hold on;
    end
    
    xlim([0 25]);
    ylim([0 27]);

end


%% Numerical Solution with SPEED
clear a b;

station=[49:-1:1];

for k=1:length(station)
    if station(k)<10
        filename_read=[DirectoryName,'monitor0000',num2str(station(k)),'.d'];
    else
        filename_read=[DirectoryName,'monitor000',num2str(station(k)),'.d'];
    end
    a=load(filename_read);
    
    t = a(:,1);
    dt = t(2) - t(1);
    ux = a(:,2);
    uy = a(:,3);
    uz = a(:,4);
    
    if filtra == 1
        fnyq=1/2/dt;
        Nfft=length(uy);
        df=1/Nfft/dt;
        freq=[0:df:(Nfft-1)*df];
        Wn = [freq_low freq_high]/fnyq;
        [c,d] = butter(n,Wn);
        
        uy = filter(c,d,uy);
    end
    plot(t-3.50,uy.*(-.5).*scala+(k+1)/2,'r-','LineWidth',1.5);hold on;
    xlim([0 25]);
    ylim([0 25.6]);
    
    if k==1
        xlabel('Time [s]','FontSize',12);
    elseif k==length(station)
        title('Valley - SH \gamma = 0','FontSize',12);
    end
end

    ylabel('u_y(x,0,0,t)','FontSize',12,'HorizontalAlignment','right','Rotation',90);
       
       
%% Semi-Analytical Solution from Sesma&Luzon X-Component
file_nameYY='SINTSH0.SXY';
filename_read=['SesmaLuzon/',file_nameYY];
a = load(filename_read);

% Parameters SesmaLuzon 
dt=0.62992;
t=[0:255].*dt;
scala = 1;
filtra = 0;
n = 3;
freq_low=.01; freq_high=0.7;


figure(11);
for k=1:(length(a)/256)
    a_ini=(k-1)*256+1;
    a_end=k*256;
    
    t_int=interp(t,50);
    dt = t_int(2) - t_int(1);
    b = interp(a(a_ini:a_end),50);
    
    if filtra == 1
        fnyq=1/2/dt;
        Nfft=length(b);
        df=1/Nfft/dt;
        freq=[0:df:(Nfft-1)*df];
        Wn = [freq_low freq_high]/fnyq;
        [c,d] = butter(n,Wn);
        
        b = filter(c,d,b);
    end
    
    if (k>1) && (k<(length(a)/256))
        plot(t_int,b+k/2,'b-','LineWidth',2);hold on;
    end
    
    xlim([0 25]);
    ylim([0 27]);

end


%% Numerical Solution with SPEED
clear a b;
station=[49:1:98];


for k=1:length(station)
    if station(k)<10
        filename_read=[DirectoryName,'monitor0000',num2str(station(k)),'.d'];
    else
        filename_read=[DirectoryName,'monitor000',num2str(station(k)),'.d'];
    end
    a=load(filename_read);
    
    t = a(:,1);
    dt = t(2) - t(1);
    ux = a(:,2);
    uy = a(:,3);
    uz = a(:,4);
    
    if filtra == 1
        fnyq=1/2/dt;
        Nfft=length(uy);
        df=1/Nfft/dt;
        freq=[0:df:(Nfft-1)*df];
        Wn = [freq_low freq_high]/fnyq;
        [c,d] = butter(n,Wn);
        
        uy = filter(c,d,uy);
    end
    plot(t-3.50,uy.*(-.5).*scala+(k+1)/2,'r-','LineWidth',1.5);hold on;
    xlim([0 25]);
    ylim([0 25.6]);
    
    if k==1
        xlabel('Time [s]','FontSize',12);
    elseif k==length(station)
        title('Valley - SH \gamma = 0','FontSize',12);
    end
end

ylabel('u_y(0,y,0,t)','FontSize',12,'HorizontalAlignment','right','Rotation',90);
       
       

