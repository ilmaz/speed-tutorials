%**************************************************************************
% Copyright (C) 2012 The SPEED FOUNDATION
% Author: SPEED-Team (POLIMI)
%         Politecnico di Milano
%         P.zza Leonardo da Vinci, 32
%         20133 Milano
%         Italy
%
% This file is part of SPEED.
%
% SPEED is free software; you can redistribute it and/or modify it
% under the terms of the GNU Affero General Public License as
% published by the Free Software Foundation, either version 3 of the
% License, or (at your option) any later version.
%
% SPEED is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with SPEED.  If not, see <http://www.gnu.org/licenses/>.
%**************************************************************************
%% ConvertMONITORS(pathIN,pathOUT)
%
%    Converts time histories obatined by SPEED in the files monitorxxxxx.d
%    where xxxxx is the monitor number in the LS.input file. The files
%    monitorxxxxx.d are formatted in the following way (by column)
%    time-instants displacement(X) displacement(Y) displacement(Z).
%
%    INPUT:
%       pathIN   : directory where SPEED output files are saved
%       (string)
%
%    OUTPUT:
%       pathOUT  : directory where SPEED monitor files are saved
%       (string)
%
% e.g.: 
% ConvertMONITORS('Input/2ConformingDG/MONITORS/','Input/2ConformingDG/monitor.d/')
%
%
%%

function ConvertMONITORS(pathIN,pathOUT)

mkdir(pathOUT);


INFO = load([char(pathIN),'MONITOR.INFO']);
t0 = 0;
T = INFO(1,1);                             %final time
dt_s = INFO(2,1);                          %deltat simulation
ndt_monit = INFO(3,1);
dt = ndt_monit*dt_s;                       %deltat monitor
MPI_num_proc = INFO(4,1);                  %number of mpi proc
MPI_mnt_id = INFO([5:5+MPI_num_proc-1],1); %id mpi for monitors
MPI_vec = [1:MPI_num_proc];
rewrite_MonitorFormat(MPI_vec,MPI_mnt_id,pathIN,pathOUT)





