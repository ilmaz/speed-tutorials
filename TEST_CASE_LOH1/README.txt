OUTPUTS:

SPEED will generate automatically three different category of files

- Files with extension ".mpi"  will be stored (if specified in SPEED.input, see online documentation) in the folder 
FILE_MPI otherwise will be sorted in the working directory. Such files are needed for managing the parallel computation and
can be used for furhter runs (see Further Runs).

- Files with extension ".input" are created during the first run and are read in successive runs. 

- Files MONITOR*.INFO e MONITOR*.D (e/o .V,.A,.S,.E,.O) contain the numerical solution and are stored in the folder
MONFILE (if specified in SPEED.input, see online documentation) otherwise will be stored in the working directory. 


POST-PROCESSING:
Copy all files contained in the folder MONFILE into the folder POST-PROC/MONITORS and run the Matlab script MAIN.m.


FURTHER RUNS: 
If you want to run other simulations using the same grid, the same polynomial approximation order and the same number of 
parallel cores and you want to save computational time, do not remove the *.mpi files generated during
the first run.

