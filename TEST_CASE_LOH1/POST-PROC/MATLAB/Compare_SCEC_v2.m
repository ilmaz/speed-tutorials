clear all;
close all;
warning off;

% FOR THE EXPORT
% Size of the image in time:
% Width =  25 cm
% Height = 20 cm


% Size of the image in frequency:
% Length = 15 cm
% Height = 20 cm


%SCEC filter definition for receiver 10
T   = 0.1;
sig = 0.055;
% sig = 0.05; %The original was 0.5; we decided to increase the sharpness of the filter!!!
ts  = 4*sig;
nr  = 10;

%%%%%%%%%% Analytic Solution %%%%%%%%%%%%%%%%%%%%%
get_analytic_SCEC;

cd ..
cd MATLAB

t_end = find(t<=9); t_end = t_end(end);
indx=(101:t_end);
figure(1), subplot(311), hold on
plot(t_ref(indx),ra_ref(indx),'k',[0,1],[0,0],'k','LineWidth',0.5)
plot(t_ref(indx),ra_ref(indx),'k','LineWidth',0.5)
figure(1), subplot(312), hold on
plot(t_ref(indx),tr_ref(indx),'k',[0,1],[0,0],'k','LineWidth',0.5)
plot(t_ref(indx),tr_ref(indx),'k','LineWidth',0.5)
figure(1), subplot(313), hold on
plot(t_ref(indx),ve_ref(indx),'k',[0,1],[0,0],'k','LineWidth',0.5)
plot(t_ref(indx),ve_ref(indx),'k','LineWidth',0.5)
filename  = cell(1);
plotcolor = cell(1); 


%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
figure(3)
t = t_ref(indx);
dt = t_ref(102)-t_ref(101);
disp = ra_ref(indx);
length_t=length(t);
df = 1/(length_t*dt);
freq = ([1:length_t/2+1]-1).*df;
DISP = abs(dt*fft(disp));
DISP = sgolayfilt(DISP,3,5);
subplot(311); loglog(freq,DISP(1:length_t/2+1)','k','LineWidth',0.5);
hold on;
%--------------------------------------------------------------------------
disp = tr_ref(indx);
DISP = abs(dt*fft(disp));
DISP = sgolayfilt(DISP,3,5);
subplot(312); loglog(freq,DISP(1:length_t/2+1)','k','LineWidth',0.5);
hold on;
%--------------------------------------------------------------------------
disp = ve_ref(indx);
DISP = abs(dt*fft(disp));
DISP = sgolayfilt(DISP,3,5);
subplot(313); loglog(freq,DISP(1:length_t/2+1)','k','LineWidth',0.5);
hold on;
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% REFERENCE DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename{1} = 'dummy';
counter = 1;
filename{counter} = ''; %input('Give filename of SCEC reference seismogram data: ','s');
i                 = 10; %input('Give station number: ');

while size(filename{counter},2)>0
    
    fid=fopen(filename{counter});
    A=fscanf(fid,'%g',[3]);
    nr=A(1);nt=A(2);dt=A(3);
    x=fscanf(fid,'%g',[nr]);
    y=fscanf(fid,'%g',[nr]);
    t=fscanf(fid,'%g',[nt,nr]);
    vx=fscanf(fid,'%g',[nt,nr]);
    vy=fscanf(fid,'%g',[nt,nr]);
    vz=fscanf(fid,'%g',[nt,nr]);
    fclose(fid);
    
    decon_source;
    
    plotcolor{counter} = input('Specify plot color: ','s');
    
    figure(1), subplot(311), hold on
    eval(['plot(t(:,1),v_rad(:,i),''',plotcolor{counter},''')']);
    text(0.5,1.9,'Radial','FontSize',12) %ylabel('radial-velocity','FontSize',14)
    axis([0 9 -2. 2.5])
    figure(1), subplot(312), hold on
    eval(['plot(t(:,1),v_trans(:,i),''',plotcolor{counter},''')']);
    ylabel('velocity[m/s]','FontSize',12), text(0.5,1.5,'Transverse','FontSize',12)
    axis([0 9 -2. 2.])
    figure(1), subplot(313), hold on
    eval(['plot(t(:,1),v_vert(:,i),''',plotcolor{counter},''')']);
    xlabel('time [s]','FontSize',12), text(0.5,1.5,'Vertical','FontSize',12)
    axis([0 9 -2. 2.]),  %ylabel('vertical-velocity','FontSize',14)
    
    %%%%%%%%% error %%%%%%%%%%%%%%%
    yi = interp2(t(:,1),v_rad(:,i),t_ref(indx),'spline');
    errorL2 = sum((yi-ra_ref(indx)).^2);
    misfitr  = errorL2/sum(ra_ref(indx).^2);
    subplot(311), text(7,1.9,['E = ',num2str(misfitr)],'FontSize',12)
    yi = interp2(t(:,1),v_trans(:,i),t_ref(indx),'spline');
    errorL2 = sum((yi-tr_ref(indx)).^2);
    misfitt  = errorL2/sum(tr_ref(indx).^2); 
    subplot(312), text(7,1.5,['E = ',num2str(misfitt)],'FontSize',12)
    yi = interp2(t(:,1),v_vert(:,i),t_ref(indx),'spline');
    errorL2 = sum((yi-ve_ref(indx)).^2);
    misfitv  = errorL2/sum(ve_ref(indx).^2); 
    subplot(313), text(7,1.5,['E = ',num2str(misfitv)],'FontSize',12)
    %disp([misfitr, misfitt,misfitv]);
    
    counter = counter + 1;
    filename{counter} = input('Give filename of SCEC reference seismogram data: ','s');

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% OWN DATA %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%filename{counter} = input('Give filename of your seismogram data: ','s');
filename{counter} = ['MONITORS_'];

if size(filename{counter},2)>0
    
    cd ..
    eval(['load ',filename{counter},'u.dat']);
    eval(['load ',filename{counter},'v.dat']);
    eval(['load ',filename{counter},'w.dat']);
    eval(['t =  ',filename{counter},'u(:,1);']);
    eval(['vx = ',filename{counter},'u(:,2:end);']);
    eval(['vy = ',filename{counter},'v(:,2:end);']);
    eval(['vz = ',filename{counter},'w(:,2:end);']);
    
    cd MATLAB
    
    %resampling to 
    dt = t(2)-t(1); 
    nt = length(t);
    
    decon_source;
    
    plotcolor{counter} = 'b'; %input('Specify plot color: ','s');
    
    %set(figure(1),'LineWidth',2)
    
    figure(1), subplot(311), hold on
    eval(['plot(t(:,1),v_rad(:,i),''',plotcolor{counter},''',''LineWidth'',2)']);
    text(0.5,1.5,'Radial','FontSize',12) %ylabel('radial-velocity','FontSize',14)
    axis([0 9 -2. 2.])
    figure(1), subplot(312), hold on
    eval(['plot(t(:,1),v_trans(:,i),''',plotcolor{counter},''',''LineWidth'',2)']);
    ylabel('velocity[m/s]','FontSize',12), text(0.5,1.5,'Transverse','FontSize',12)
    axis([0 9 -2. 2.])
    figure(1), subplot(313), hold on
    eval(['plot(t(:,1),v_vert(:,i),''',plotcolor{counter},''',''LineWidth'',2)']);
    xlabel('time [s]','FontSize',12), text(0.5,1.5,'Vertical','FontSize',12)
    axis([0 9 -2. 2.]) %ylabel('vertical-velocity')
    
    

    
    %--------------------------------------------------------------------------
    %--------------------------------------------------------------------------
    %--------------------------------------------------------------------------
    figure(3)
    dt = t(2,1)-t(1,1);
    disp = v_rad(:,i);
    length_t=length(t);
    df = 1/(length_t*dt);
    freq = ([1:length_t/2+1]-1).*df;
    DISP = abs(dt*fft(disp));
    DISP = sgolayfilt(DISP,3,5);
    subplot(311);
    eval(['loglog(freq,DISP(1:length_t/2+1)'',''',plotcolor{counter},''',''LineWidth'',2)']);
    axis([0 10 0.01 1]);
    hold on; grid on;
    xlim([0 10]);ylim([0.01 1]);
    %--------------------------------------------------------------------------
    disp = v_trans(:,i);
    DISP = abs(dt*fft(disp));
    DISP = sgolayfilt(DISP,3,5);
    subplot(312);
    eval(['loglog(freq,DISP(1:length_t/2+1)'',''',plotcolor{counter},''',''LineWidth'',2)']);
    ylabel('Power Spectrum','FontSize',12)
    axis([0 10 0.01 1]);
    hold on; grid on;
    xlim([0 10]);ylim([0.01 1]);
    %--------------------------------------------------------------------------
    disp = v_vert(:,i);
    DISP = abs(dt*fft(disp));
    DISP = sgolayfilt(DISP,3,5);
    subplot(313);
    eval(['loglog(freq,DISP(1:length_t/2+1)'',''',plotcolor{counter},''',''LineWidth'',2)']);
    xlabel('frequency [Hz]','FontSize',12)
    hold on; grid on;
    xlim([0 10]);ylim([0.01 1]);
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------
    %----------------------------------------------------------------------

    %%%%%%%%% error %%%%%%%%%%%%%%%
    yi = interp1(t(:,1),v_rad(:,i),t_ref(indx),'spline');
    errorL2 = sum((yi-ra_ref(indx)).^2);
    misfitr  = errorL2/sum(ra_ref(indx).^2); 
    figure(1), subplot(311), text(7,1.9,['E = ',num2str(misfitr)],'FontSize',12)
    
    vel_x = yi;
    vel_ref_x = ra_ref(indx);
    
    yi = interp1(t(:,1),v_trans(:,i),t_ref(indx),'spline');
    errorL2 = sum((yi-tr_ref(indx)).^2);
    misfitt  = errorL2/sum(tr_ref(indx).^2); 
    figure(1), subplot(312), text(7,1.5,['E = ',num2str(misfitt)],'FontSize',12)
    
    vel_y = yi;
    vel_ref_y = tr_ref(indx);
    
    yi = interp1(t(:,1),v_vert(:,i),t_ref(indx),'spline');
    errorL2 = sum((yi-ve_ref(indx)).^2);
    misfitv  = errorL2/sum(ve_ref(indx).^2); 
    figure(1), subplot(313), text(7,1.5,['E = ',num2str(misfitv)],'FontSize',12)
    
    vel_z = yi;
    vel_ref_z = ve_ref(indx);
    
    tempo=linspace(0,9,1025);
    
%     counter = counter + 1;
%     filename{counter} = input('Give filename of your seismogram data: ','s');
%     
%     datafilename =  [filename{counter-1}, '.data'];
%     file_id = fopen(datafilename, 'w');
%     for i = 1: 1025
%         fprintf(file_id, '%10.8e   %10.8e   %10.8e  %10.8e \n', tempo(i), vel_x(i), vel_y(i), vel_z(i));
%     end
%     fclose(file_id);

end

% datafilename =  ['REFERENCE', '.data'];
% file_id = fopen(datafilename, 'w');
% for i = 1: 1025
%     fprintf(file_id, '%10.8e  %10.8e   %10.8e   %10.8e \n', tempo(i), vel_ref_x(i), vel_ref_y(i), vel_ref_z(i));
% end
% fclose(file_id);
% 
% 
z% disp(' '), disp('Finished!'), disp(' ')