% *******************************************************
% ***              COMPARISON ON EXPLOSION TETST      ***
% ***                          .m                     ***
% *******************************************************
% *** author: Marco Stupazzini                        *** 
% *** SPICE Post Doc                                  ***

% *** Department of Earth and Environmental Sciences  ***
% *** Chair of Geophysics                             *** 
% *** Munich University                               *** 
% *** Theresienstr. 41                                *** 
% *** 80333 Munich                                    *** 
% *** Germany                                         *** 
% *** tel.: +49 (89) 2180-4143                        *** 
% *** fax.: +49 (89) 2180-4205                        *** 
% *** e-mail: stupa@geophysik.uni-muenchen.de         *** 

% *** date:   29 October 2006                         ***

function [] = READ_LOH1_AND_REWRITE_Martin_FORMAT(pathMain,sub_directory_1st)

clc;
warning off;





% ***********************************************************
% ***                PARAMETER DECLARATION                ***
% ***********************************************************
% ***                                                     ***

passo = 1;

frequenza = 30;
n = 3; %Wn = [1000 30000]/fnyq;

scala = 1;
receivers_coord = load([pathMain,'receivers.txt']);

directory_1st = pathMain;


% ***                                                     ***
% ***                                                     ***
% ***********************************************************


directory_1st_tot = [char(directory_1st),char(sub_directory_1st),'/'];


%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------


for i = 1:10
        disp(['i = ',char(num2str(i))]);
        if i<10
            a=load([char(directory_1st_tot),'monitor0000',char(num2str(i)),'.d']);
        else
            a=load([char(directory_1st_tot),'monitor000',char(num2str(i)),'.d']);
        end   
        
        dt = a(2,1) - a(1,1);
        t= a(:,1);
        
        ux = 10^18*a(:,2);
        uy = 10^18*a(:,3);
        uz = 10^18*a(:,4);
        
        %------------------------------------------------------------------
        % Filtro Butterworth passabanda del III ordine nella banda definita
        % da Wn
        fnyq=1/2/dt;
        Nfft=length(ux);
        %Nfft = 12771;
        %Nfft = 17131;
        %Nfft = 2200;
        %Nfft = 14120;
        df=1/Nfft/dt;
        freq=[0:df:(Nfft-1)*df];
        Wn = [frequenza]/fnyq;
        [c,d] = butter(n,Wn,'low');
        
        uxf = filtfilt(c,d,ux);
        uyf = filtfilt(c,d,uy);
        uzf = filtfilt(c,d,uz); 
        %------------------------------------------------------------------
        
        vx = zeros(Nfft,1);
        vx(1) = 0;
        vx(2:Nfft-1) = (ux(3:Nfft) - ux(1:Nfft-2))./(2*dt);
        vx(Nfft) = 0;
        %vx(1:Nfft-2) = (uxf(3:Nfft) - uxf(1:Nfft-2))./(2*dt);
        %vx(Nfft-1) = 0;
        %vx(Nfft) = 0;
        
        vy = zeros(Nfft,1);
        vy(1) = 0;
        vy(2:Nfft-1) = (uy(3:Nfft) - uy(1:Nfft-2))./(2*dt);
        vy(Nfft) = 0;
        %vy(1:Nfft-2) = (uyf(3:Nfft) - uyf(1:Nfft-2))./(2*dt);
        %vy(Nfft-1) = 0;
        %vy(Nfft) = 0;
        
        vz = zeros(Nfft,1);
        vz(1) = 0;
        vz(2:Nfft-1) = (uz(3:Nfft) - uz(1:Nfft-2))./(2*dt);
        vz(Nfft) = 0;
        %vz(1:Nfft-2) = (uzf(3:Nfft) - uzf(1:Nfft-2))./(2*dt);
        %vz(Nfft-1) = 0;
        %vz(Nfft) = 0;
        
        %------------------------------------------------------------------
        % Filtro Butterworth passabanda del III ordine nella banda definita
        % da Wn
        fnyq=1/2/dt;
        %Nfft=length(ux);
        df=1/Nfft/dt;
        freq=[0:df:(Nfft-1)*df];
        Wn = [frequenza]/fnyq;
        [c,d] = butter(n,Wn,'low');
        
        
        vxf = filtfilt(c,d,vx);
        vyf = filtfilt(c,d,vy);
        vzf = filtfilt(c,d,vz); 
        %------------------------------------------------------------------
        
        ax(:,i) = vx;  
        ay(:,i) = vy;
        az(:,i) = vz;  
        
end  

%-------------------------------------------------------------------------
%-------------------------------------------------------------------------
%-------------------------------------------------------------------------

file_name = [char(sub_directory_1st),char('_u.dat')];
%file_name='loh1_P3_fine_stupa_original_u.dat';
%file_name='loh1_P4_coarse_stupa_filtered_u.dat';
nomefilew=[char(directory_1st_tot),char(file_name)];
fid = fopen(nomefilew,'w');
fprintf(fid,'%+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e\n',...
                               [t(1:passo:Nfft)'; ax(1:passo:Nfft,1)'; ax(1:passo:Nfft,2)';...
                                    ax(1:passo:Nfft,3)'; ax(1:passo:Nfft,4)'; ax(1:passo:Nfft,5)';...
                                    ax(1:passo:Nfft,6)'; ax(1:passo:Nfft,7)'; ax(1:passo:Nfft,8)';...
                                    ax(1:passo:Nfft,9)'; ax(1:passo:Nfft,10)']);
fclose(fid);

file_name = [char(sub_directory_1st),char('_v.dat')];
%file_name='loh1_P4_coarse_stupa_original_v.dat';
%file_name='loh1_P4_coarse_stupa_filtered_v.dat';
nomefilew=[char(directory_1st_tot),char(file_name)];
fid = fopen(nomefilew,'w');
fprintf(fid,'%+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e\n',...
                                [t(1:passo:Nfft)'; ay(1:passo:Nfft,1)'; ay(1:passo:Nfft,2)';...
                                    ay(1:passo:Nfft,3)'; ay(1:passo:Nfft,4)'; ay(1:passo:Nfft,5)';...
                                    ay(1:passo:Nfft,6)'; ay(1:passo:Nfft,7)'; ay(1:passo:Nfft,8)';...
                                    ay(1:passo:Nfft,9)'; ay(1:passo:Nfft,10)']);
fclose(fid);

file_name = [char(sub_directory_1st),char('_w.dat')];
%file_name='loh1_P4_coarse_stupa_original_w.dat';
%file_name='loh1_P4_coarse_stupa_filtered_w.dat';
nomefilew=[char(directory_1st_tot),char(file_name)];
fid = fopen(nomefilew,'w');
fprintf(fid,'%+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e %+13.7e\n',...
                                [t(1:passo:Nfft)'; az(1:passo:Nfft,1)'; az(1:passo:Nfft,2)';...
                                    az(1:passo:Nfft,3)'; az(1:passo:Nfft,4)'; az(1:passo:Nfft,5)';...
                                    az(1:passo:Nfft,6)'; az(1:passo:Nfft,7)'; az(1:passo:Nfft,8)';...
                                    az(1:passo:Nfft,9)'; az(1:passo:Nfft,10)']);
fclose(fid);

