% COMPARISON FOR SCEC LOH1 TEST 
%
% Ref. Day S.M., Bradley C.R. 2001. Memory-efficient simulation 
% of anelastic wave propagation. Bulletin of the
% Seismological Society of America, 91(3): 520–531.
%


% REWRITE SPEED OUTPUT FILES STORED IN THE FOLDER path 
% USING OPTIONS OPT_OUT according to SPEED.input file. 
% OPT_OUT(i) = 0/1 -> NO/YES
% i = 1 DISPLACEMENT
% i = 2 VELOCITY
% i = 3 ACCELERATION
% i = 4 STRESS TENSOR
% i = 5 STRAIN TENSOR
% i = 6 ROTATIONAL TENSOR

path = '../MONITORS/';
OPT_OUT = [1 0 0 0 0 0];
REWRITE_MONITOR_FORMAT(path,OPT_OUT);



% REWRITE OUTPUT FILE IN A FORMAT COMPATIBLE TO Martin Kaser's 
% routine Cmpare_SCEC_v2 for comparing semi-analytical vs synthetic 
% solution.

pathMain = '';
sub_directory_1st = '../MONITORS';

READ_LOH1_AND_REWRITE_Martin_FORMAT(pathMain,sub_directory_1st);


% Compare Solutions recorded at reciever R10 = (6000,8000,0)m 
Compare_SCEC_v2