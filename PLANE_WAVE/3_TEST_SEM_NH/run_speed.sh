#!/bin/bash

export SPEED_PROGRAM=SPEED_ifc
export OUTPUT=speed.out 
export TOTAL_MPI_PROCESSES=2
export THREADS=1
 
let NCORES=$TOTAL_MPI_PROCESSES
echo "Running on $NCORES cores"
echo "Job started at `date`"

time mpirun -np $TOTAL_MPI_PROCESSES  -x OMP_NUM_THREADS=$THREADS  $SPEED_PROGRAM >& $OUTPUT
wait

