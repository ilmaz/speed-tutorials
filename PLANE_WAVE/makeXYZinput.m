% *******************************************************
% *******************************************************
% ***                                                 ***
% ***                  make_XYZinput.m                ***
% *******************************************************
% *** author: SPEED-Team                              ***
% *** POLIMI                                          ***
% *******************************************************
% *** Politecnico di Milano                           ***
% *** P.zza Leonardo da Vinci, 32                     ***
% *** 20133 Milano                                    ***
% *** Italy                                           ***
% *******************************************************

clear; clc;  warning off;
tstart=cputime;

%%%%%%%%%%%%%%TO BE FILLED BY USER%%%%%%%%%%%%%%%%%%%%%%%
%Directory containing FileName.txt 
path='PLANE_WAVE/INPUTS/3_TEST_SEM_NH/'; 

%FileName
file_name='XYZ';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nomefiler=[file_name,'.txt'];
nomefilew=[file_name,'.out'];

fid = fopen(nomefiler,'r');

tab=7;
chexa  = 0;
ctetra = 0;
cpyram = 0;
cquad  = 0;
ctria  = 0;

num_chexa  = 0;
num_cpyram  = 0;
num_ctetra = 0;

num_cquad=0;
num_ctria=0;

%leggo le prime 8 righe (prime 7 inutili )
for i= 1:6
    tline = fgetl(fid);
end
%leggo numero nodi
num_nodes = str2num(tline(20-tab:length(tline)-1));
tline = fgetl(fid);
%leggo numero elementi
num_elem = str2num(tline(19-tab:length(tline)-1)); 
tline = fgetl(fid);
%leggo numero blocchi
num_el_blk = str2num(tline(21-tab:length(tline)-1));
%leggo riga inutile
tline = fgetl(fid);
%tline = fgetl(fid)

j=1;
%ciclo sui blocchi
for i=1:num_el_blk
    string = tline(2:8)
    if(strcmp(string,'num_el_'));
        %numero di elementi nel blocco j
        num_el_in_blk(j)=str2num(tline(25-tab+fix(j/10):length(tline)-1));
        tline = fgetl(fid);
        string = tline(2:8);
    end
   j=j+1; 
    if(strcmp(string,'num_nod'))
        tline = fgetl(fid);
        string = tline(2:8);
    end
%     if(strcmp(string,'num_nod'));
%         %guardo quanti nodi ha ogni elemento del blocco j
%         num_nod_per_el(j)=str2num(tline(26-tab+fix(j/10):length(tline)-1));
%         if num_nod_per_el(j)==8
%             num_chexa = num_chexa +  num_el_in_blk(j);
%         elseif num_nod_per_el(j)==3
%             num_ctria = num_ctria +  num_el_in_blk(j);
%         else
%             num_ctetra=num_ctetra+num_el_in_blk(j); %PROBLEMA COME DISTINGUO TETRAEDRI DA QUADRATI????????'!!!!!!
%         end
%         j = j + 1;
%         tline = fgetl(fid);
%         string = tline(2:8);
%     end
    
    if(strcmp(string,'num_att'))
        tline = fgetl(fid);
        string = tline(2:8);
    end
    
end
while(strcmp( tline(3:9) ,'connect') ~=1)
    tline=fgetl(fid);
end
%tipo_elementi
% 1 HEXA
% 2 TETRA
% 3 PYRA
% 4 QUAD
% 5 TRI
%%%%%%%%%%%%%%%
%% Number of elements in a block
for i=1:num_el_blk
    
    if (strcmp(tline( 25+fix(i/10) : length(tline)-3),'HEX8' ))
        type_elem(i) = 1;
        num_chexa    = num_chexa + num_el_in_blk(i);
    elseif (strcmp(tline( 25+fix(i/10) : length(tline)-3),'TETRA' ))
        type_elem(i) = 2;
        num_ctetra   = num_ctetra + num_el_in_blk(i);
    elseif (strcmp(tline( 25+fix(i/10) : length(tline)-3),'PYRAMID5' ))
        type_elem(i) = 3;
        num_cpyram   = num_cpyram + num_el_in_blk(i);
    elseif (strcmp( tline( 25+fix(i/10) : length(tline)-3),'TRI3' ))
        type_elem(i) = 5;
        num_ctria    = num_ctria + num_el_in_blk(i);
    else
        type_elem(i) = 4;
        num_cquad    = num_cquad + num_el_in_blk(i);
    end
    tline=fgetl(fid);
    string=tline(9:14);
    if(strcmp(string,'attrib'))
        tline=fgetl(fid);
    end
    tline=fgetl(fid);
end


%numero di elementi totali per ogni tipo
disp(['hexa     : ', num2str(num_chexa)]);
disp(['tetra    : ', num2str(num_ctetra)]);
disp(['pyramids : ', num2str(num_cpyram)]);
disp(['quads    : ', num2str(num_cquad)]);
disp(['tria     : ', num2str(num_ctria)]);

disp(['type_element :' num2str(type_elem)]);


%% Node Coordinates
grid_id=zeros(num_nodes,1);

tini=cputime;
['BEGIN - Reading nodes coordinates']

grid=num_nodes;
grid_id=[1:num_nodes];

% Opening whole file as charecter array
mesh_dat = fileread(nomefiler);
dum_ind = strfind(mesh_dat,';');


% X-coordinates
ind_node_start = strfind( mesh_dat,' coordx =') + 9;
srch_ind = find(dum_ind >= ind_node_start);
ind_node_end = dum_ind(srch_ind(1)) - 1;
dum = regexprep(strtrim(mesh_dat(ind_node_start:ind_node_end)),'[\n\r]+','');
grid_x = str2num(dum)';

% Y-coordinates
ind_node_start = strfind( mesh_dat,' coordy =') + 9;
srch_ind = find(dum_ind >= ind_node_start);
ind_node_end = dum_ind(srch_ind(1)) - 1;
dum = regexprep(strtrim(mesh_dat(ind_node_start:ind_node_end)),'[\n\r]+','');
grid_y = str2num(dum)';

% Z-coordinates
ind_node_start = strfind( mesh_dat,' coordz =') + 9;
srch_ind = find(dum_ind >= ind_node_start);
ind_node_end = dum_ind(srch_ind(1)) - 1;
dum = regexprep(strtrim(mesh_dat(ind_node_start:ind_node_end)),'[\n\r]+','');
grid_z = str2num(dum)';

clear dum ind_node_start ind_node_end mesh_dat srch_ind dum_ind

['END - Reading nodes coordinates in ',char(num2str(cputime-tini)),' sec.']


%%

l_tline=8;
tline = fgetl(fid);
i=0;

%matrici di connettività volume
con_chexa  = zeros(num_chexa,8);
con_ctetra = zeros(num_ctetra,4);
con_cpyram = zeros(num_cpyram,5);

%matrici di connettività superfici
con_cquad = zeros(num_cquad,4);
con_ctria = zeros(num_ctria,3);


%matrici con tag e id
chexa_tag = zeros(num_chexa,1);
chexa_id  = zeros(num_chexa,1);

ctetra_tag = zeros(num_ctetra,1);
ctetra_id  = zeros(num_ctetra,1);

cpyram_tag = zeros(num_ctetra,1);
cpyram_id  = zeros(num_ctetra,1);

cquad_tag  = zeros(num_cquad,1);
cquad_id   = zeros(num_cquad,1);

ctria_tag  = zeros(num_ctria,1);
ctria_id   = zeros(num_ctria,1);


chexa_1 = zeros(num_chexa,1);
chexa_2 = zeros(num_chexa,1);
chexa_3 = zeros(num_chexa,1);
chexa_4 = zeros(num_chexa,1);
chexa_5 = zeros(num_chexa,1);
chexa_6 = zeros(num_chexa,1);
chexa_7 = zeros(num_chexa,1);
chexa_8 = zeros(num_chexa,1);

ctetra_1 = zeros(num_ctetra,1);
ctetra_2 = zeros(num_ctetra,1);
ctetra_3 = zeros(num_ctetra,1);
ctetra_4 = zeros(num_ctetra,1);

cpyram_1 = zeros(num_cpyram,1);
cpyram_2 = zeros(num_cpyram,1);
cpyram_3 = zeros(num_cpyram,1);
cpyram_4 = zeros(num_cpyram,1);
cpyram_5 = zeros(num_cpyram,1);

cquad_1 = zeros(num_cquad,1);
cquad_2 = zeros(num_cquad,1);
cquad_3 = zeros(num_cquad,1);
cquad_4 = zeros(num_cquad,1);

ctria_1 = zeros(num_ctria,1);
ctria_2 = zeros(num_ctria,1);
ctria_3 = zeros(num_ctria,1);


%riempio matrici di connettività, tag e id
for i=1:num_el_blk
    
    if i==1
        tline = fgetl(fid);
        ['Reading ',char(num2str(i)),'st block  (',char(num2str(num_el_in_blk(i))),' elem.)']
    elseif i==2
        ['completed in ',char(num2str(cputime-tini)),' sec.']
        ['Reading ',char(num2str(i)),'nd block  (',char(num2str(num_el_in_blk(i))),' elem.)']
    elseif i==3
        ['completed in ',char(num2str(cputime-tini)),' sec.']
        ['Reading ',char(num2str(i)),'rd block  (',char(num2str(num_el_in_blk(i))),' elem.)']
    else
        ['completed in ',char(num2str(cputime-tini)),' sec.']
        ['Reading ',char(num2str(i)),'th block  (',char(num2str(num_el_in_blk(i))),' elem.)']
    end
    
    tini=cputime;
    
    %salto righe inutili
    while strcmp(tline(1:l_tline),' connect')~=1
        tline = fgetl(fid);
        if length(tline)<8
            l_tline=length(tline);
        elseif isempty(tline)
            l_tline=1;
        else
            l_tline=8;
        end
    end
    
    
    if type_elem(i)==1
        for j=1:num_el_in_blk(i)
            tline = fgetl(fid);
            %pos=findstr(tline,',');
            chexa=chexa+1;
            chexa_id(chexa)=chexa;
            chexa_tag(chexa)=i;
            con_chexa(chexa,:)=str2num(tline);
        end
        
    elseif type_elem(i)==2
        for j=1:num_el_in_blk(i)
            tline = fgetl(fid);
            %pos=findstr(tline,',');
            ctetra=ctetra+1;
            ctetra_id(ctetra)=ctetra;
            ctetra_tag(ctetra)=i;
            con_ctetra(ctetra,:)=str2num(tline);
        end
    elseif type_elem(i)==3
        for j=1:num_el_in_blk(i)
            tline = fgetl(fid);
            %pos=findstr(tline,',');
            cpyram=cpyram+1;
            cpyram_id(cpyram)=cpyram;
            cpyram_tag(cpyram)=i;
            con_cpyram(cpyram,:)=str2num(tline);
        end
    elseif type_elem(i)==4
        for j=1:num_el_in_blk(i)
            tline = fgetl(fid);
            %pos=findstr(tline,',');
            cquad=cquad+1;
            cquad_id(cquad)=cquad;
            cquad_tag(cquad)=i;
            con_cquad(cquad,:)=str2num(tline);
        end
    else
        for j=1:num_el_in_blk(i)
            tline=fgetl(fid);
            ctria=ctria+1;
            ctria_id(ctria)=ctria;
            ctria_tag(ctria)=i;
            con_ctria(ctria,:)=str2num(tline);
        end
    end
   
end

['completed in ',char(num2str(cputime-tini)),' sec.']

tini=cputime;
['*** Storing data informations ***']
% connettività di ogni vertice
if chexa>0
    chexa_1(1:chexa)=con_chexa(1:chexa,1);
    chexa_2(1:chexa)=con_chexa(1:chexa,2);
    chexa_3(1:chexa)=con_chexa(1:chexa,3);
    chexa_4(1:chexa)=con_chexa(1:chexa,4);
    chexa_5(1:chexa)=con_chexa(1:chexa,5);
    chexa_6(1:chexa)=con_chexa(1:chexa,6);
    chexa_7(1:chexa)=con_chexa(1:chexa,7);
    chexa_8(1:chexa)=con_chexa(1:chexa,8);
end
if ctetra > 0
    ctetra_1(1:ctetra)=con_ctetra(1:ctetra,1);
    ctetra_2(1:ctetra)=con_ctetra(1:ctetra,2);
    ctetra_3(1:ctetra)=con_ctetra(1:ctetra,3);
    ctetra_4(1:ctetra)=con_ctetra(1:ctetra,4);
end
if cpyram > 0
    cpyram_1(1:cpyram)=con_cpyram(1:cpyram,1);
    cpyram_2(1:cpyram)=con_cpyram(1:cpyram,2);
    cpyram_3(1:cpyram)=con_cpyram(1:cpyram,3);
    cpyram_4(1:cpyram)=con_cpyram(1:cpyram,4);
    cpyram_5(1:cpyram)=con_cpyram(1:cpyram,5);
end
if cquad>0
    cquad_1(1:cquad)=con_cquad(1:cquad,1);
    cquad_2(1:cquad)=con_cquad(1:cquad,2);
    cquad_3(1:cquad)=con_cquad(1:cquad,3);
    cquad_4(1:cquad)=con_cquad(1:cquad,4);
end

if ctria > 0
    ctria_1(1:ctria)=con_ctria(1:ctria,1);
    ctria_2(1:ctria)=con_ctria(1:ctria,2);
    ctria_3(1:ctria)=con_ctria(1:ctria,3);
end


['completed in ',char(num2str(cputime-tini)),' sec.']



%---------------------------------------------------------------


fid = fopen(nomefilew,'w');

tini=cputime;
['BEGIN - Writing inp format']

fprintf(fid,'   %i   %i   \n',[grid; ctria]);

if grid>0
    fprintf(fid,'%i  %+13.7e  %+13.7e  %+13.7e \n',[grid_id(1:grid); grid_x(1:grid)'; grid_y(1:grid)'; grid_z(1:grid)']);
end
if ctria > 0
    fprintf(fid,'%i %i %i %i %i\n',...
        [ctria_id(1:ctria)'; ...
        ctria_1(1:ctria)';ctria_2(1:ctria)';ctria_3(1:ctria)';ctria_tag(1:ctria)']);
end
fclose(fid);



['END - Writing inp format in ',char(num2str(cputime-tini)),' sec.']
['TOTAL CPU time ',char(num2str(cputime-tstart)),' sec.']
fclose(fid);


