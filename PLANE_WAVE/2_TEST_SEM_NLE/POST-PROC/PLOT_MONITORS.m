%% PLOT THE RESULTS
clear all
%close all
clc

%% TO BE DEFINED BY THE USER

path1 = '../MONITOR/';   %directory where monitor files are stored


t0 = 0;              %Start Time
T = 10;              %Final Time
Num_of_tot_mon = 4;  %Number of monitors

%%


for i = 1 : Num_of_tot_mon
    
    if i < 10
        fileName = ['monitor0000',num2str(i),'.d'];
    elseif i < 100
        fileName = ['monitor000',num2str(i),'.d'];
    elseif i < 1000
        fileName = ['monitor00',num2str(i),'.d'];
    elseif i < 10000
        fileName = ['monitor0',num2str(i),'.d'];
    elseif i < 100000
        fileName = ['monitor',num2str(i),'.d'];
    end
    
    sol_1 = load([path1,fileName]);
    
    
    figure(i)
    subplot(311)
    hold on
    plot(sol_1(:,1),sol_1(:,2),'r-','LineWidth',2);hold on; grid on;
    xlim([t0 T]);
    ylim([-5 5]);
    xlabel('t (s)');
    ylabel('u_x (m)');
    legend('SEM-NLE');
    title(fileName)
    
    subplot(312)
    plot(sol_1(:,1),sol_1(:,3),'r-','LineWidth',2);hold on; grid on;
    xlim([t0 T]);
    ylim([-5 5]);
    xlabel('t (s)');
    ylabel('u_y (m)');
    legend('SEM-NLE')
    
    subplot(313)
    hold on
    plot(sol_1(:,1),sol_1(:,4),'r-','LineWidth',2);hold on; grid on;
    xlim([t0 T]);
    ylim([-5 5]);
    xlabel('t (s)');
    ylabel('u_z (m)');
    legend('SEM-NLE')
    
    
end
